  --
  
  MELOMPATI, ver/I
 
  Experimental ELF ASM x86,x64 "jump" Hunter for JOP purposes
  
  @; tsar.in, 2012.
  
  --  

  Disclaimer: Melompati is still in a experimental version to demonstrate techniques to hunt JOP gadgets. This version is not currently suitable to analyze large programs. In next versions we'll be more solid.

  --

  For MELOMPATI works well, you need install distorm Python library:

  URL: http://code.google.com/p/distorm/
  
  --
  
  JOP: Jump-Oriented Programming
  
  JOP is a new class of code-reuse attack against buffer overflow vulnerabilities.
  
  For more info about JOP and techniques, search for papers like "noret.pdf" and "ASIACCS11.pdf".
  
  --
  
  @eremitah
  
  --
  
