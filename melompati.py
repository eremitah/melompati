#!/usr/bin/python
# -*- coding: UTF-8 -*-
""""""
"""
||
|| MELOMPATI, serie/I
|| 
|| Experimental ELF ASM x86,x64 "jump" Hunter for JOP purposes
|| 
|| @; eremitah, tsar.in, 2012.
||
"""
""""""
""""""
import sys
""""""
""""""
PYVER = sys.version.split()[0]
""""""
""""""
if PYVER >= "3" or PYVER < "2.6":
    from lib.default import error
    error("-- error: your Python version is not compatible.", True)
else:
    from lib.core import main
    main()
""""""
""""""
