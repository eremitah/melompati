#!/usr/bin/python
# -*- coding: UTF-8 -*-
""""""
"""
"" heuristica.py -- Heuristic routines to find and validate gadgets
"" This code is a part of MELOMPATI project. 
"""
""""""
""""""
import re, sys
from string import lower
from lib.default import JUMPS, REGS, GGT_HIGH, GGT_MID, GGT_NO
""""""
""""""
class MelompatiHEUR:
    """"""
    def __init__(self, outfile = None, linhas = 0, opt = None):    
        self.linhas = linhas
        self.outfile = outfile
        self.opt = opt
        return
    """"""
    def Search(self, instr = None):
        ret = lower(instr).split()
        if ret[0] in JUMPS and (ret[1][1:3] != "0x" and ret[1][0:2] != "0x"):
            return True
        else:
            return False
    """"""
    def Remap(self, gadget, ptr):
        addr = []
        instr = []
        for li in gadget[0][ptr:]:
            addr.append(li)
        for li in gadget[1][ptr:]:
            instr.append(li)
        return [addr,instr]
    """"""
    def Reuse(self, gadget):
        ggt = []
        for li in gadget:
            if len(li[1]) > 1:
                ret = [li[0],li[1]]
                ggt.append(ret)
            else:
                pass
        return ggt
    """"""
    def MakeGadget(self, gadgets = None):
        linhas = []
        instr = []
        addr = []        
        gadget = []

        for li in gadgets:
            linhas.append(li[0])

        fd = open(self.outfile, 'r').readlines()

        for i in linhas:
            for li in fd[i-int(self.linhas):i]:
                tmp = li[:-1].split("|")
                addr.append(tmp[0])
                instr.append(tmp[1][:-1] if tmp[1][-1] == ' ' else tmp[1])
            ret = [addr,instr]
            gadget.append(ret)
            instr = []
            addr = []
        return gadget
    """"""
    def Heuristica(self, gadgets):
        i = 0
        y = 0
        ptr = 0
        level = {}
        
        for li in gadgets: 
            for le in li[1][0:len(li[1])-1]:
                tmp = le.split()                
                if lower(tmp[0]) in GGT_HIGH:                
                    level[i] = 2
                elif lower(tmp[0]) in GGT_MID:
                    level[i] = 1
                elif lower(tmp[0]) in GGT_NO or lower(tmp[0]) in JUMPS:                    
                    level[i] = 0
                    ptr = i+1
                else:
                    level[i] = 0
                    ptr = i+1
                i+=1
            if ptr > 0:
                gadgets[y] = self.Remap(li, ptr)
            
            level = {}            
            i = 0
            ptr = 0
            y+=1
        
        return self.Reuse(gadgets)
    """"""  
    def Catalogo(self, gadgets):
        return
    """"""
""""""
""""""
""""""
        
    
