#!/usr/bin/python
# -*- coding: UTF-8 -*-
""""""
"""
"" readelf.py -- ELF file analyzer.
"" This code is a part of MELOMPATI project. 
"""
""""""
""""""
import struct, sys
from lib.default import error
""""""
""""""
class MelompatiELF:
    """"""
    def __init__(self, arquivo = None):
        self.arquivo = arquivo
        self.os = 0
        self.elf = {'.data':    [0x00000000, 0x0],
                    '.plt':     [0x00000000, 0x0],
                    '.got':     [0x00000000, 0x0],
                    '.got.plt': [0x00000000, 0x0],
                    '.bss':     [0x00000000, 0x0],
                    '.comment': [0x00000000, 0x0],
                    '.text':    [0x00000000, 0x0],
                    '.init':    [0x00000000, 0x0],
                    'base':     0x00000000,
                    'entry':    0x00000000,
                    'desc':     None,
                    'hash':     None}
        return
    """"""
    def Open(self):
        self.fd = open(self.arquivo, 'rb')
        return
    """"""
    def ReadELFHeader(self):
        self.f = self.fd        

        elf = {'ident_data':None,
               'fmt':None,
               'elf_class':None,
               'data':None }
        
        fmt = {'ident':'16s', 
               '32':'HHIIIIIHHHHHH', 
               '64':'HHIQQQIHHHHHH' }
        
        estrutura = ['e_ident', 'e_type', 'e_machine', 'e_version', 'e_entry',
                  'e_phoff', 'e_shoff', 'e_flags', 'e_ehsize', 'e_phentsize', 
                  'e_phnum', 'e_shentsize', 'e_shnum', 'e_shstrndx']
        
        self.f.seek(0)
        elf['ident_data'] = self.f.read(struct.calcsize(fmt['ident']))
        elf['fmt'] = None
        
        if ord(elf['ident_data'][4]) == 1:
            elf['elf_class'] = 32
            elf['fmt'] = fmt['32']
            elf['data'] = self.f.read(struct.calcsize(fmt['32']))
            
        elif ord(elf['ident_data'][4]) == 2:
            elf['elf_class'] = 64
            elf['fmt'] = fmt['64']
            elf['data'] = self.f.read(struct.calcsize(fmt['64']))
            
        if ord(elf['ident_data'][5]) == 1:
            elf['fmt'] = '<' + fmt['ident'] + elf['fmt']
            
        elif ord(elf['ident_data'][5]) == 2:
            elf['fmt'] = '>' + fmt['ident'] + elf['fmt']
        
        self.os = elf['elf_class']
        
        try:        
            elf = dict(zip(estrutura,struct.unpack(elf['fmt'],elf['ident_data']+elf['data'])))
            self.elf['entry'] = "0x%08x" % elf['e_entry']
            if self.IsElf(elf['e_ident']) == False:
                error("-- error: this file is not a ELF file.", True)
        except:
            error("-- error: can't unpack and build up ELF dict.", True)
            
        return elf
    """"""
    def ReadSectionHeader(self):
        self.f = self.fd
        
        fmt = {'32':'IIIIIIIIII', 
               '64':'IIQQQQIIQQ' }
        
        elf = {'data':None, 
               'sh_hdrs':[], 
               'shstrndx_hdr':None, 
               'shstr': None}
        
        estrutura = ['sh_name_idx', 'sh_type', 'sh_flags', 'sh_addr', 'sh_offset','sh_size', 
                  'sh_link', 'sh_info', 'sh_addralign', 'sh_entsize']
        
        self.f.seek(self.header['e_shoff'])
        
        for sh in xrange(self.header['e_shnum']):
            try:
                elf['data'] = self.f.read(self.header['e_shentsize'])
                tmp = dict(zip(estrutura, struct.unpack(fmt[str(self.os)],elf['data'])))
                elf['sh_hdrs'].append(tmp)
            except:
                pass
            
        elf['shstrndx_hdr'] = elf['sh_hdrs'][self.header['e_shstrndx']]
        self.f.seek(elf['shstrndx_hdr']['sh_offset'])
        elf['shstr'] = self.f.read(elf['shstrndx_hdr']['sh_size'])    

        for h in elf['sh_hdrs']:            
            offset = h['sh_name_idx']
            h['sh_name'] = elf['shstr'][offset:offset+elf['shstr'][offset:].index(chr(0x0))]

            if h['sh_name'] == '.text':
                self.elf['base']  = "0x%08x" % (h['sh_addr'] - h['sh_offset'])                                            
                self.elf['.text'] = "0x%08x" % h['sh_addr']
            else:
                if h['sh_name'] in ['.data','.init','.bss','.plt','.got','.got.plt']:
                    self.elf[h['sh_name']] = ["0x%08x" % h['sh_addr'], "0x%x" % h['sh_offset']]
                elif h['sh_name'] == '.comment':
                    self.elf[h['sh_name']] = "0x%08x" % (int(eval(self.elf['base'])) + h['sh_offset'])            
                else:
                    pass            
                
        return elf['sh_hdrs']
    """"""
    def GetHeaders(self):
        self.Start()        
        return self.elf, self.os
    """"""
    def IsElf(self, opt):
        if opt[1:4] == 'ELF': return True
        return False
    """"""
    def Start(self):
        self.Open()
        self.header = self.ReadELFHeader()
        self.section = self.ReadSectionHeader()  
    """"""
""""""
""""""
""""""
