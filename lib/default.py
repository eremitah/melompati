# -*- coding: UTF-8 -*-
""""""
"""
"" default.py -- Global routines and functions
"" This code is a part of MELOMPATI project. 
"""
""""""
""""""
import sys
""""""
""""""
__VER__  = "Ia"
__DATA__ = "20121017"
""""""
""""""

REGS = [['32',['eax','ebx','ecx','edx','esi','edi','ebp','esp','eip']],
        ['64',['eax','ebx','ecx','edx','esi','edi','ebp','esp',
               'rax','rbx','rcx','rdx','rsi','rdi','rbp','rsp',
               'r8','r9','r10','r11','r12','r13','r14','r15','rip']]]

JUMPS = ['jmp','ljmp',
         'je','ja','jle','jg','js','jz',
         'jne','jnz',
         'jbe']

GGT_HIGH = ['mov','movl',
            'add','addl','adc',
            'sub','subl',
            'xchg',
            'pop','popa','popad',
            'push','pushf',
            'sbb',
            'cmp','cmpl','cmpb','compsl']

GGT_MID = ['and','xor','or',
           'div','idiv','fdiv',
           'db',
           'aam',
           'test',
           'std',
           'inc','dec',
           'int1',
           'lodsb','insb','movsb',
           'in',                  
           'nop',
           'loopnz',
           'clc','cld','cmc','hlt',
           'out']

GGT_NO = ['jmp']
""""""
""""""
def Help():
    print "??\n?? [*] Help: \n??"
    print "??   -b, --bin   : ELF file to be analyzed and dissected"
    print "??   -l, --lines : max lines when find some jump in/condition\t[1..10]"
    print "??   -d, --dec   : select ELF decode type for 32 ou 64-bit\t[32/64]"
    print "??   -o, --outf  : outfile to save all gadgets"
    print "??   -p, --print : print all gadgets found"
    print "??\n--"
    error(False, True)
""""""
""""""
def error(msg = None, opt = False):
    if msg:
        print "{0}".format(str(msg))
    if opt == True:
        sys.exit(1)
    else:
        return True
    return False
""""""
""""""
def Formata(opt):
    ret = None
    addr = opt[0][0]
    for li in opt[1]:
        ret = (str(ret) + " ; " if ret != None else "") + str(li)
    ret = str(ret) + " ;"
    if len(opt[1]) < 2:
        return [None,None]
    return [addr, ret]
""""""
""""""
def Topo():
    from lib.default import __VER__
    print "||\n|| MELOMPATI, ver/{0} -- Experimental ELF ASM x86,x64 \"jump\" Hunter for JOP purposes".format(__VER__)
    print "|| @; eremitah, tsar.in, 2012."
    print "||\n--"
    return
""""""
""""""
def Aviso():
    error("?? syntax: type \"-h\" or \"--help\"", True)
""""""
""""""
""""""
