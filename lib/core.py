#!/usr/bin/python
# -*- coding: UTF-8 -*-
""""""
"""
"" core.py -- CORE controller and ignitor
"" This code is a part of MELOMPATI project. 
"""
""""""
""""""
from lib.default import error, Topo
import lib.opts as opts
""""""
""""""
class Melompati:
    """"""
    def __init__(self, argv = False):
        Topo()
        ret = opts.CheckOpt(argv)
        if ret == False:
            error("-- exiting.", True)
        else:                
            if ret['bin']:
                try:
                    import lib.bin as _bin
                    pati = _bin.MelompatiBIN(ret)
                    pati.Start()
                except:
                    error("-- error: can't load and start library \"lib/bin.py\"", True)
                error("-- EOF.", True)         
            else:
                error("-- error: you need specify one binary file (\"-b\" or \"--bin\").", True)
        return
    """"""
""""""
""""""
def main():
    import sys
    lompat = Melompati(sys.argv)
    return
""""""
""""""
if __name__ == '__main__':
    main()
""""""
""""""
""""""
