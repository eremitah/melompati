# -*- coding: UTF-8 -*-
""""""
"""
"" opts.py -- ARGV parser.
"" This code is a part of MELOMPATI project. 
"""
""""""
""""""
import getopt
from lib.default import Aviso, Help
try:
    from distorm import Decode32Bits,Decode64Bits
except:
    error("-- error: can't find distorm Python library.", True)
""""""
""""""
def CheckOpt(opt):
    dec = {'32': Decode32Bits, 
           '64': Decode64Bits }
    
    ret = {'txt': None,
           'bin': None,
           'lines': 4,
           'deep': 4,
           'offset': 0,
           'dec': dec['32'],
           'outf': None,
           'print': False }    
    
    if len(opt) < 2:
        Aviso()
    
    optl, args = getopt.getopt(opt[1:], ":h1:t:b:l:d:o:p1:", ['help','txt=','bin=','dec=','lines=','outf=','print'])
    for x,y in optl:
        if x in ('-h','--help'): Help()
        elif x in ('-t','--txt'): ret['txt'] = y
        elif x in ('-b','--bin'): ret['bin'] = y
        elif x in ('-l','--lines'): ret['lines'] = int(y)
        elif x in ('-d','--dec'): ret['dec'] = dec[y]
        elif x in ('-o','--outf'): ret['outf'] = str(y)
        elif x in ('-p','--print'): ret['print'] = True
        else: pass        
    return ret
""""""    
""""""
