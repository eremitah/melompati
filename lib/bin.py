#!/usr/bin/python
# -*- coding: UTF-8 -*-
""""""
"""
"" bin.py -- Disassemble binary routines
"" This code is a part of MELOMPATI project. 
"""
""""""
""""""
from lib.default import JUMPS, REGS, GGT_HIGH, GGT_MID, GGT_NO, Formata, error
import lib.heuristica as _heur
""""""
""""""
import os, sys, re, string
try:
    import distorm
except:
    error("-- error: can't find distorm Python library.", True)
import hashlib
""""""
""""""
class MelompatiBIN:
    """"""
    def __init__(self, opts):
        self.opts = opts
        self.decode = opts['dec']
        self.offset = opts['offset'] if opts['offset'] else 0
        self.arquivo = opts['bin']
        self.linhas = opts['lines'] + 1        
        self.fd = None
        self.tempfile = None
        self.info = {}
        return
    """"""
    def Open(self, fd):
        try:
            self.fd = open(fd, 'rb')
        except:
            error("-- error: can't open file.", True)
        return
    """"""
    def Disasm(self):
        content = self.fd.read()
        disasm = distorm.DecodeGenerator(self.offset, content, self.decode)
        return disasm
    """"""
    def Gerador(self):
        temp = []
        gadgets = []
        linha = 1
        content = self.fd.read()
        b_size = 1024 * 1024
        b_count = (len(content)/b_size) + 1
        
        for c in xrange(b_count):
            b_start = c * b_size
            
            disasm = distorm.DecodeGenerator(b_start, content[b_start:b_start + b_size], self.decode)
            
            for (offset, size, instr, hexa) in disasm:               
                temp.append("{0}|{1}\n".format(self._set_addr(offset), instr)) 

                if _heur.MelompatiHEUR(None).Search(instr):
                    ret = [linha, self._set_addr(offset), instr, hexa]
                    gadgets.append(ret) 

                linha+=1
        
        self.tempfile = "/tmp/{0}-{1}.tmp".format(sys.argv[0],self.info['hash'])
        self.Save(temp)
        temp = []
        
        return gadgets
    """"""
    def Info(self, opt = None):
        if opt == 'md5':
            fd = open(self.arquivo, 'rb')
            md5 = hashlib.md5()
            md5.update(fd.read())
            fd.close()            
            return md5.hexdigest()
        
        elif opt == 'basename':
            return os.path.basename(self.arquivo)
        
        elif opt == 'addrs':
            import lib.readelf as _elf
            return _elf.MelompatiELF(self.arquivo).GetHeaders()
        
        else:
            return None
        
        return None
    """"""
    def Status(self, gadgets, heuristica = None, catalogo = None):
        print ">>"
        print ">> ELF file: {0:32} Type: {1:2}-bits".format(self.arquivo,self.os)
        print ">> MD5 sum:  {0:32}".format(self.info['hash'])
        print ">>\n>> Binary header and sections address:\n>>"        
        print ">>   entry_ptr: {0:10}    base_addr: {1:10}\n>>       .data: {2:10}         .bss: {3:10}".format(self.info['entry'],self.info['base'],self.info['.data'][0],self.info['.bss'][0])
        print ">>"
        print ">> Total of preliminaries gadgets found is: {0}".format(len(gadgets))
        print ">>"
        print ">>         Potential gadgets : {0}".format(len(heuristica))

        if self.opts['outf']:
            self.Log(gadgets)
        
        if self.opts['print'] == True:
            print "--"
            print "<<"
            print "<< Showing all gadgets:\n<<"            
            for li in gadgets:
                x,y = Formata(li)
                self.Print(x,y)
        return
    """"""
    def Print(self, addr = None, instr = None):
        if addr != None and instr != None:
            print "<< {0}: {1}".format(addr,string.lower(instr))
        return
    """"""
    def Load(self):
        return
    """"""
    def Save(self, temp):        
        outf = open(self.tempfile,'w')
        outf.write("".join(temp))
        outf.close()
        return  
    """"""
    def Remove(self):
        os.system("rm %s" % self.tempfile)
        return
    """"""
    def Log(self, gadgets):        
        outb = []
        msg = """##\n# MELOMPATI, ver/I -- JOP potential gadgets outfile\n#\n#   file: {0}\n# md5sum: {1}\n#""".format(self.arquivo, self.info['hash'])        
        tempfile = "{0}.pati".format(self.opts['outf'])
        print ">>\n>> Saving gadgets in outfile : {0}\n>>".format(tempfile)
        try:
            outf = open(tempfile,'w')
        except:
            error("-- error: can't open outfile", False)            
        outb.append(msg)
        for li in gadgets:
            x,y = li
            for i in xrange(len(x)):
                outb.append("{0}: {1}".format(x[i], string.lower(y[i])))
            outb.append("--")            
        outf.write("\n".join(outb))
        outf.close()
        return        
    """"""
    def _set_addr(self, offset):
        return "0x%08x" % int(eval(self.info['base']) + offset)
    """"""
    def Start(self): 
        gadgets = []
        heuristica = []
        catalogo = []
        _gadgets = {}
        
        self.Open(self.arquivo)
        
        self.info, self.os = self.Info('addrs')
        self.info['hash'] = self.Info('md5')
        self.info['desc'] = self.Info('basename')  

        gadgets = self.Gerador()

        gadgets = _heur.MelompatiHEUR(self.tempfile, self.linhas, None).MakeGadget(gadgets)
        heuristica = _heur.MelompatiHEUR(None).Heuristica(gadgets)
        #catalogo = _heur.MelompatiHEUR(None).Catalogo(heuristica)
        
        self.Status(gadgets,heuristica,catalogo)        

        """ disasm simples """
        """
        disasm = None
        disasm = self.Disasm()
        for q,w,e,r in disasm:
            self.Print(q,e)
        """
        
        self.Remove()        
        return
    """"""
""""""
""""""
""""""
