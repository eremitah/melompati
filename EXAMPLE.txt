[mao@mao v1]$ ./melompati.py
||
|| MELOMPATI, ver/I -- Experimental ELF ASM x86,x64 "jump" Hunter for JOP purposes
|| @; eremitah, tsar.in, 2012.
||
--
?? syntax: type "-h" or "--help"
[mao@mao v1]$ ./melompati.py -h
||
|| MELOMPATI, ver/I -- Experimental ELF ASM x86,x64 "jump" Hunter for JOP purposes
|| @; eremitah, tsar.in, 2012.
||
--
??
?? [*] Help: 
??
??   -b, --bin   : ELF file to be analyzed and dissected
??   -l, --lines : max lines when find some jump in/condition	[1..10]
??   -d, --dec   : select ELF decode type for 32 ou 64-bit	[32/64]
??   -o, --outf  : outfile to save all gadgets
??   -p, --print : print all gadgets found
??
--
[mao@mao v1]$ ./melompati.py -b sample.bin32 -d 32 -l 7
||
|| MELOMPATI, ver/I -- Experimental ELF ASM x86,x64 "jump" Hunter for JOP purposes
|| @; eremitah, tsar.in, 2012.
||
--
>>
>> ELF file: sample.bin32                     Type: 32-bits
>> MD5 sum:  8d41a9e44b75cee1d5ef3101685d58a1
>>
>> Binary header and sections address:
>>
>>   entry_ptr: 0x08048300    base_addr: 0x08048000
>>       .data: 0x080496b8         .bss: 0x080496c0
>>
>> Total of preliminaries gadgets found is: 10
>>
>>   Potential gadgets      : 10
>>
-- EOF.
[mao@mao v1]$ ./melompati.py -b sample.bin64 -d 64 -l 4 -p
||
|| MELOMPATI, ver/I -- Experimental ELF ASM x86,x64 "jump" Hunter for JOP purposes
|| @; eremitah, tsar.in, 2012.
||
--
>>
>> ELF file: sample.bin64                     Type: 64-bits
>> MD5 sum:  188e7b6764b3c25ffabea7b3e60dc2f7
>>
>> Binary header and sections address:
>>
>>   entry_ptr: 0x00400410    base_addr: 0x00400000
>>       .data: 0x006008e0         .bss: 0x006008f0
>>
>> Total of preliminaries gadgets found is: 15
>>
>>   Potential gadgets      : 13
>>
--
<<
<< Showing all gadgets:
<<
<< 0x004003ca: add [rax], al ; add [rax], al ; add [rax], al ; push qword [rip+0x2004e2] ; jmp [rip+0x2004e4] ;
<< 0x004003dc: nop dword [rax+0x0] ; jmp [rip+0x2004e2] ;
<< 0x00400461: pop rbp ; mov edi, 0x6008f0 ; jmp rax ;
<< 0x004004a1: pop rbp ; mov edi, 0x6008f0 ; jmp rdx ;
<< 0x0040062b: add [rax+rax], bl ; add [rax], al ; loopnz 0x62f ; db 0xff ; jmp far dword [rdx] ;
<< 0x0040065e: add [rax], al ; pushf ; db 0xfe ; db 0xff ; jmp far dword [rip+0x0] ;
<< 0x00400744: add [rax], al ; add [rax], al ; cmc ; db 0xfe ; jmp far dword [rdi+0x0] ;
<< 0x00400813: add [rax], al ; add [rax], al ; add dh, bh ; db 0xff ; jmp far dword [rdi+0x0] ;
<< 0x00400824: add [rax], al ; add [rax], al ; db 0xff ; db 0xff ; jmp far dword [rdi+0x0] ;
<< 0x00400833: add [rax], al ; add [rax], al ; add al, dh ; db 0xff ; jmp far dword [rdi+0x0] ;
<< 0x00400b2e: add [rax], al ; add [rax], r8b ; add dh, dh ; db 0xff ; jmp far dword [rdi+0x2] ;
<< 0x00400bef: add [rsi+0x0], bl ; add [rax], al ; db 0xff ; db 0xff ; jmp far dword [rdi+0x2] ;
<< 0x00400c2f: add [rbx+0x0], ch ; add [rax], al ; db 0xfe ; db 0xff ; jmp far dword [rdi+0x2] ;
-- EOF.
[mao@mao v1]$ ./melompati.py -b /bin/ls -d 64 -l 5 -o /tmp/outfile
||
|| MELOMPATI, ver/I -- Experimental ELF ASM x86,x64 "jump" Hunter for JOP purposes
|| @; eremitah, tsar.in, 2012.
||
--
>>
>> ELF file: /bin/ls                          Type: 64-bits
>> MD5 sum:  b644436213727325a293e98b11e4e613
>>
>> Binary header and sections address:
>>
>>   entry_ptr: 0x00404888    base_addr: 0x00400000
>>       .data: 0x0061a380         .bss: 0x0061a5e0
>>
>> Total of preliminaries gadgets found is: 192
>>
>>         Potential gadgets : 75
>>
>> Saving gadgets in outfile : /tmp/outfile.pati
>>
-- EOF.
[mao@mao v1]$ cat /tmp/outfile.pati 
##
# MELOMPATI, ver/I -- JOP potential gadgets outfile
#
#   file: /bin/ls
# md5sum: b644436213727325a293e98b11e4e613
#
0x00400e11: add [rax], al
0x00400e13: add [rax], al
0x00400e15: add [rax], al
0x00400e17: add [rax+0x100000], bh
0x00400e1e: int1
0x00400e1f: jmp rax
--
0x00400e41: add [rax], al
0x00400e43: add [rax], al
0x00400e45: add [rax], al
0x00400e47: add [rip+0x10000005], dh
0x00400e4d: add cl, dh
0x00400e4f: jmp rax
--
0x004021aa: add [rax], al
0x004021ac: add [rax], al
0x004021ae: add [rax], al
0x004021b0: push qword [rip+0x217e3a]
0x004021b6: jmp [rip+0x217e3c]
--
0x004021bc: nop dword [rax+0x0]
0x004021c0: jmp [rip+0x217e3a]
--
0x004021d0: jmp [rip+0x217e32]
--
0x004021e0: jmp [rip+0x217e2a]
--
0x004021f0: jmp [rip+0x217e22]
--
0x00402200: jmp [rip+0x217e1a]
--
0x00402210: jmp [rip+0x217e12]
--
0x00402220: jmp [rip+0x217e0a]
--
0x00402230: jmp [rip+0x217e02]
--
0x00402240: jmp [rip+0x217dfa]
--
0x00402250: jmp [rip+0x217df2]
--
0x00402260: jmp [rip+0x217dea]
--
0x00402270: jmp [rip+0x217de2]
--
0x00402280: jmp [rip+0x217dda]
--
0x00402290: jmp [rip+0x217dd2]
--
0x004022a0: jmp [rip+0x217dca]
--
0x004022b0: jmp [rip+0x217dc2]
--
0x004022c0: jmp [rip+0x217dba]
--
0x004022d0: jmp [rip+0x217db2]
--
0x004022e0: jmp [rip+0x217daa]
--
0x004022f0: jmp [rip+0x217da2]
--
0x00402300: jmp [rip+0x217d9a]
--
0x00402310: jmp [rip+0x217d92]
--
0x00402320: jmp [rip+0x217d8a]
--
0x00402330: jmp [rip+0x217d82]
--
0x00402340: jmp [rip+0x217d7a]
--
0x00402350: jmp [rip+0x217d72]
--
0x00402360: jmp [rip+0x217d6a]
--
0x00402370: jmp [rip+0x217d62]
--
0x00402380: jmp [rip+0x217d5a]
--
0x00402390: jmp [rip+0x217d52]
--
0x004023a0: jmp [rip+0x217d4a]
--
0x004023b0: jmp [rip+0x217d42]
--
0x004023c0: jmp [rip+0x217d3a]
--
0x004023d0: jmp [rip+0x217d32]
--
0x004023e0: jmp [rip+0x217d2a]
--
0x004023f0: jmp [rip+0x217d22]
--
0x00402400: jmp [rip+0x217d1a]
--
0x00402410: jmp [rip+0x217d12]
--
0x00402420: jmp [rip+0x217d0a]
--
0x00402430: jmp [rip+0x217d02]
--
0x00402440: jmp [rip+0x217cfa]
--
0x00402450: jmp [rip+0x217cf2]
--
0x00402460: jmp [rip+0x217cea]
--
0x00402470: jmp [rip+0x217ce2]
--
0x00402480: jmp [rip+0x217cda]
--
0x00402490: jmp [rip+0x217cd2]
--
0x004024a0: jmp [rip+0x217cca]
--
0x004024b0: jmp [rip+0x217cc2]
--
0x004024c0: jmp [rip+0x217cba]
--
0x004024d0: jmp [rip+0x217cb2]
--
0x004024e0: jmp [rip+0x217caa]
--
0x004024f0: jmp [rip+0x217ca2]
--
0x00402500: jmp [rip+0x217c9a]
--
0x00402510: jmp [rip+0x217c92]
--
0x00402520: jmp [rip+0x217c8a]
--
0x00402530: jmp [rip+0x217c82]
--
0x00402540: jmp [rip+0x217c7a]
--
0x00402550: jmp [rip+0x217c72]
--
0x00402560: jmp [rip+0x217c6a]
--
0x00402570: jmp [rip+0x217c62]
--
0x00402580: jmp [rip+0x217c5a]
--
0x00402590: jmp [rip+0x217c52]
--
0x004025a0: jmp [rip+0x217c4a]
--
0x004025b0: jmp [rip+0x217c42]
--
0x004025c0: jmp [rip+0x217c3a]
--
0x004025d0: jmp [rip+0x217c32]
--
0x004025e0: jmp [rip+0x217c2a]
--
0x004025f0: jmp [rip+0x217c22]
--
0x00402600: jmp [rip+0x217c1a]
--
0x00402610: jmp [rip+0x217c12]
--
0x00402620: jmp [rip+0x217c0a]
--
0x00402630: jmp [rip+0x217c02]
--
0x00402640: jmp [rip+0x217bfa]
--
0x00402650: jmp [rip+0x217bf2]
--
0x00402660: jmp [rip+0x217bea]
--
0x00402670: jmp [rip+0x217be2]
--
0x00402680: jmp [rip+0x217bda]
--
0x00402690: jmp [rip+0x217bd2]
--
0x004026a0: jmp [rip+0x217bca]
--
0x004026b0: jmp [rip+0x217bc2]
--
0x004026c0: jmp [rip+0x217bba]
--
0x004026d0: jmp [rip+0x217bb2]
--
0x004026e0: jmp [rip+0x217baa]
--
0x004026f0: jmp [rip+0x217ba2]
--
0x0040271e: db 0xff
0x0040271f: db 0xff
0x00402720: jmp [rip+0x217b8a]
--
0x00402730: jmp [rip+0x217b82]
--
0x00402740: jmp [rip+0x217b7a]
--
0x00402750: jmp [rip+0x217b72]
--
0x00402760: jmp [rip+0x217b6a]
--
0x00402770: jmp [rip+0x217b62]
--
0x00402780: jmp [rip+0x217b5a]
--
0x00402790: jmp [rip+0x217b52]
--
0x004027a0: jmp [rip+0x217b4a]
--
0x004027b0: jmp [rip+0x217b42]
--
0x004027c0: jmp [rip+0x217b3a]
--
0x004027d0: jmp [rip+0x217b32]
--
0x004027e0: jmp [rip+0x217b2a]
--
0x004027f0: jmp [rip+0x217b22]
--
0x00402800: jmp [rip+0x217b1a]
--
0x00402810: jmp [rip+0x217b12]
--
0x00402820: jmp [rip+0x217b0a]
--
0x00402830: jmp [rip+0x217b02]
--
0x00402840: jmp [rip+0x217afa]
--
0x00402850: jmp [rip+0x217af2]
--
0x00402860: jmp [rip+0x217aea]
--
0x00402870: jmp [rip+0x217ae2]
--
0x00402880: jmp [rip+0x217ada]
--
0x00402890: jmp [rip+0x217ad2]
--
0x004028a0: jmp [rip+0x217aca]
--
0x00402b78: nop dword [rax+rax+0x0]
0x00402b80: jmp [rax*8+0x412bd0]
--
0x004044da: mov eax, edx
0x004044dc: jmp [rax*8+0x413468]
--
0x004048e1: pop rbp
0x004048e2: mov edi, 0x61a5e0
0x004048e7: jmp rax
--
0x00404921: pop rbp
0x00404922: mov edi, 0x61a5e0
0x00404927: jmp rdx
--
0x00404ba3: jmp [rcx*8+0x4127e0]
--
0x00407bdf: push r12
0x00407be1: push rbp
0x00407be2: push rbx
0x00407be3: sub rsp, 0x38
0x00407be7: mov eax, [rip+0x212a67]
0x00407bed: jmp [rax*8+0x412ba8]
--
0x0040db43: nop dword [rax+rax+0x0]
0x0040db48: mov eax, [rsp+0x48]
0x0040db4c: jmp [rax*8+0x416380]
--
0x0040e08c: jmp [rax*8+0x4163c8]
--
0x0040f1db: jmp [rdx*8+0x416848]
--
0x00410a7c: nop dword [rax+0x0]
0x00410a80: jmp [r12*8+0x416ea8]
--
0x004112a8: jmp [r12*8+0x416f78]
--
0x004117dd: jmp [r12*8+0x417198]
--
0x00417386: add [rax], al
0x00417388: xor al, 0xb5
0x0041738a: db 0xfe
0x0041738b: jmp far dword [rbx+rcx+0x0]
--
0x004173af: add [rsi+rdx*8], dh
0x004173b2: db 0xfe
0x004173b3: jmp far dword [rdi+rax+0x0]
--
0x004173ef: add ah, ah
0x004173f1: db 0xdb
0x004173f2: db 0xfe
0x004173f3: jmp far dword [rax+rcx+0x0]
--
0x004173f7: add ah, dh
0x004173f9: db 0xdb
0x004173fa: db 0xfe
0x004173fb: inc dword [rax+rcx-0x23cc0000]
0x00417402: db 0xfe
0x00417403: jmp [rax+rcx-0x23bc0000]
--
0x0041742a: db 0xfe
0x0041742b: push qword [rcx+rcx]
0x0041742e: add [rax], al
0x00417430: adc al, 0xde
0x00417432: db 0xfe
0x00417433: jmp far dword [rcx+rcx+0x0]
--
0x0041744d: or [rax], eax
0x0041744f: add [rdx+0xa0cfffe], dl
0x00417456: add [rax], al
0x00417458: and al, 0xe3
0x0041745a: db 0xfe
0x0041745b: jmp [rdx+rcx]
--
0x004174ba: db 0xfe
0x004174bb: push qword [rbx+rcx-0x146c0000]
0x004174c2: db 0xfe
0x004174c3: jmp rsp
--
0x004174e7: add [rdx+rsi*8], al
0x004174ea: db 0xfe
0x004174eb: inc dword [rsp+rcx-0xdac0000]
0x004174f2: db 0xfe
0x004174f3: jmp [rsp+rcx-0xc2c0000]
--
0x0041750b: push qword [rcx-0x9bc0000]
0x00417512: db 0xfe
0x00417513: dec dword [rbp+rcx+0x0]
0x00417517: add ah, al
0x00417519: idiv dh
0x0041751b: jmp [rbp+rcx+0x0]
--
0x00417532: db 0xfe
0x00417533: jmp far dword [rbp+rcx-0x7fc0000]
--
0x00417562: db 0xfe
0x00417563: dec dword [rsi+rcx+0x0]
0x00417567: add [rdx+rdi*8], ah
0x0041756a: db 0xfe
0x0041756b: jmp [rsi+rcx+0x0]
--
0x00417582: db 0xfe
0x00417583: jmp far dword [rsi+rcx-0x4ac0000]
--
0x004175aa: db 0xfe
0x004175ab: jmp [rdi+rcx]
--
0x004175f8: db 0x64
0x004175f8: adc bh, bh
0x004175fb: dec dword [rcx+rdx+0x0]
0x004175ff: add ah, dh
0x00417601: adc bh, bh
0x00417603: jmp [rcx+rdx+0x0]
--
0x0041761a: db 0xff
0x0041761b: db 0xff
0x0041761c: mov esp, 0x34000011
0x00417621: adc edi, edi
0x00417623: jmp rsp
--
0x00417631: adc al, 0xff
0x00417633: push qword [rdx+rdx]
0x00417636: add [rax], al
0x00417638: adc al, 0x15
0x0041763a: db 0xff
0x0041763b: jmp [rdx+rdx+0x0]
--
0x00417655: adc al, [rax]
0x00417657: db 0x0
0x00417658: push rsp
0x00417659: db 0x26
0x0041765a: db 0xff
0x0041765b: jmp [rbx+rdx]
--
0x00417666: add [rax], al
0x00417668: db 0xc4
0x00417669: sub edi, edi
0x0041766b: jmp far dword [rbx+rdx+0x2c240000]
--
0x00417688: db 0x64
0x00417689: db 0x2e
0x0041768a: db 0xff
0x0041768b: inc dword [rsp+rdx+0x2f040000]
0x00417692: db 0xff
0x00417693: jmp far dword [rsp+rdx+0x30240000]
--
0x004176ba: db 0xff
0x004176bb: jmp far dword [rbp+rdx+0x31f40000]
--
0x00417723: push qword [rdi+rdx+0x0]
0x00417727: add [rbx+rdi+0x178cffff], al
0x0041772e: add [rax], al
0x00417730: xchg esp, eax
0x00417731: cmp edi, edi
0x00417733: jmp [rdi+rdx+0x3ba40000]
--
0x0041776b: push qword [rax+rbx+0x3f040000]
0x00417772: db 0xff
0x00417773: dec esp
0x00417775: sbb [rax], al
0x00417777: add [rdi+rdi-0x1], dl
0x0041777b: jmp rsp
--
0x004177a0: add al, 0x42
0x004177a2: db 0xff
0x004177a3: dec esp
0x004177a5: sbb [rax], eax
0x004177a7: add [rdx+rax*2-0x1], dh
0x004177ab: jmp rsp
--
0x004177b6: add [rax], al
0x004177b8: and al, 0x43
0x004177ba: db 0xff
0x004177bb: dec dword [rdx+rbx+0x0]
0x004177bf: add [rbx+rax*2-0x1], al
0x004177c3: jmp [rdx+rbx+0x0]
--
0x004177f8: db 0xd4
0x004177f9: db 0x4a
0x004177fa: db 0xff
0x004177fb: inc dword [rbx+rbx+0x4ca40000]
0x00417802: db 0xff
0x00417803: jmp [rbx+rbx+0x4d840000]
--
0x0041781b: push qword [rsp+rbx]
0x0041781e: add [rax], al
0x00417820: xchg esp, eax
0x00417821: push rdi
0x00417822: db 0xff
0x00417823: jmp [rsp+rbx+0x0]
--
0x0041785d: sbb eax, 0x61c40000
0x00417862: db 0xff
0x00417863: dec dword [rsi+rbx+0x0]
0x00417867: add [rdx], al
0x0041786a: db 0xff
0x0041786b: jmp far dword [rsi+rbx+0x0]
--
0x0041787b: inc dword [rdi+rbx]
0x0041787e: add [rax], al
0x00417880: hlt
0x00417881: db 0x64
0x00417882: db 0xff
0x00417883: jmp [rdi+rbx]
--
0x004178a8: test [rdi+rdi*8-0x1], dh
0x004178ac: or al, 0x20
0x004178ae: add [rax], al
0x004178b0: in al, 0x74
0x004178b2: db 0xff
0x004178b3: jmp far dword [rax]
--
0x004178cb: push qword [rax+0x0]
0x004178cf: add [rbp+rsi*2-0x1], ah
0x004178d3: dec dword [rax+0x75a40000]
0x004178da: db 0xff
0x004178db: jmp far dword [rax+0x76540000]
--
0x00417907: add [rax+rdi*2-0x1], al
0x0041790b: push qword [rcx+0x0]
0x0041790f: add [rax+rdi*2-0x1], dl
0x00417913: dec dword [rcx+0x78640000]
0x0041791a: db 0xff
0x0041791b: jmp [rcx+0x78a40000]
--
0x0041792b: jmp rsp
--
0x0041793e: add [rax], al
0x00417940: mov ah, 0x79
0x00417942: db 0xff
0x00417943: jmp far dword [rdx]
--
0x0041796b: jmp far dword [rdx+0x7aa40000]
--
0x0041799b: jmp far dword [rbx+0x0]
--
0x004179af: add [rdx+rbx*4+0x2414ffff], al
0x004179b6: add [rax], al
0x004179b8: movsb
0x004179b9: db 0x9a
0x004179ba: db 0xff
0x004179bb: jmp far dword [rsp]
--
0x004179ce: add [rax], al
0x004179d0: and al, 0x9c
0x004179d2: db 0xff
0x004179d3: inc dword [rsp-0x63bc0000]
0x004179da: db 0xff
0x004179db: jmp [rsp-0x637c0000]
--
0x004179e3: dec esp
0x004179e5: and al, 0x0
0x004179e7: add ah, dl
0x004179e9: pushf
0x004179ea: db 0xff
0x004179eb: jmp rsp
--
0x00417a02: db 0xff
0x00417a03: inc dword [rbp+0x0]
0x00417a07: add [rbp+rbx*4-0x1], dh
0x00417a0b: jmp [rbp+0x0]
--
0x00417a40: db 0xd4
0x00417a41: lodsb
0x00417a42: db 0xff
0x00417a43: inc dword [rsi-0x527c0000]
0x00417a4a: db 0xff
0x00417a4b: jmp far dword [rsi-0x523c0000]
--
0x00417a62: db 0xff
0x00417a63: jmp [rdi]
--
0x00417a93: jmp far dword [rdx]
--
0x00417f3b: jmp rdx
--
0x0041835a: add [rax], al
0x0041835c: mov esp, 0x70000008
0x00418361: clc
0x00418362: db 0xfe
0x00418363: jmp [rbp+0x4]
--
0x00418672: add [rax], al
0x00418674: db 0xd4
0x00418675: or eax, [rax]
0x00418677: add [rax+0x12], ch
0x0041867a: db 0xff
0x0041867b: jmp rdx
--
0x004186a2: add [rax], al
0x004186a4: add al, 0xc
0x004186a6: add [rax], al
0x004186a8: sub [rbx], dl
0x004186aa: db 0xff
0x004186ab: jmp rbx
--
0x004189b2: add [rax], al
0x004189b4: adc al, 0xf
0x004189b6: add [rax], al
0x004189b8: clc
0x004189b9: sbb al, 0xff
0x004189bb: jmp far dword [rax+rax]
--
0x00418a52: add [rax], al
0x00418a54: mov ah, 0xf
0x00418a56: add [rax], al
0x00418a58: test al, 0x1f
0x00418a5a: db 0xff
0x00418a5b: jmp [rax+0x0]
--
0x00418a70: xor [rax], ah
0x00418a72: db 0xff
0x00418a73: jmp far dword [rdx]
--
0x00418b3a: add [rax], al
0x00418b3c: pushf
0x00418b3d: adc [rax], al
0x00418b3f: add al, ah
0x00418b41: and edi, edi
0x00418b43: jmp [rdx]
--
0x00418c7e: add [rax], al
0x00418c80: mov al, 0x26
0x00418c82: db 0xff
0x00418c83: jmp far dword [rax]
--
0x00418d16: add [rax], al
0x00418d18: clc
0x00418d19: db 0x27
0x00418d1a: db 0xff
0x00418d1b: jmp far dword [rsi+0x0]
--
0x00418d4a: add [rax], al
0x00418d4c: lodsb
0x00418d4d: adc al, [rax]
0x00418d4f: add [rax], dh
0x00418d51: sub bh, bh
0x00418d53: jmp [rdx+0x0]
--
0x00418dd2: db 0xff
0x00418dd3: jmp [rax]
--
0x00418ddf: add [rax+rax], ch
0x00418de2: add [rax], al
0x00418de4: adc r8d, [rax]
0x00418de7: add al, bl
0x00418de9: sub bh, bh
0x00418deb: jmp far dword [rbx+0x1]
--
0x0041942c: mov word [rcx], ds
0x0041942e: add [rax], al
0x00419430: db 0xf0
0x00419430: push rsp
0x00419432: db 0xff
0x00419433: jmp [rdx+0x0]
--
0x00419792: add [rax], al
0x00419794: hlt
0x00419795: sbb al, 0x0
0x00419797: add [rax+0x76], ch
0x0041979a: db 0xff
0x0041979b: jmp [rax]
--
0x004197b2: db 0xff
0x004197b3: jmp [rbp+0x0]
--
0x0041994a: add [rax], al
0x0041994c: lodsb
0x0041994d: db 0x1e
0x0041994e: add [rax], al
0x00419950: adc [rbp-0x1], bh
0x00419953: jmp [rbp+0x4]
--
0x00419a02: add [rax], al
0x00419a04: db 0x64
0x00419a05: db 0x1f
0x00419a06: add [rax], al
0x00419a08: xchg dil, dil
0x00419a0b: jmp [rsi+0x0]
--
0x00419a4c: lodsb
0x00419a4d: db 0x1f
0x00419a4e: add [rax], al
0x00419a50: db 0xf0
0x00419a50: xchg bh, bh
0x00419a53: jmp [rbp+0x0]
--
0x00419e72: add [rax], al
0x00419e74: add [rax], al
0x00419e76: add [rax], al
0x00419e78: cmc
0x00419e79: db 0xfe
0x00419e7a: jmp far dword [rdi+0x0]
--
0x00419f41: add [rax], al
0x00419f43: add [rax], al
0x00419f45: add [rax], al
0x00419f47: add dh, bh
0x00419f49: db 0xff
0x00419f4a: jmp far dword [rdi+0x0]
--
0x00419f4f: add al, ah
0x00419f51: adc eax, 0x40
0x00419f56: add [rax], al
0x00419f58: db 0xff
0x00419f59: db 0xff
0x00419f5a: jmp far dword [rdi+0x0]
--
0x00419f61: add [rax], al
0x00419f63: add [rax], al
0x00419f65: add [rax], al
0x00419f67: add al, dh
0x00419f69: db 0xff
0x00419f6a: jmp far dword [rdi+0x0]
--
0x0041a7fc: add [rax], al
0x0041a7fe: add [rax], al
0x0041a800: xor al, 0x0
0x0041a802: add [rax], al
0x0041a804: idiv bh
0x0041a806: jmp far dword [rdi+0x2]
--
0x0041a8bc: add [rax], al
0x0041a8be: add [rax], al
0x0041a8c0: add [rax], r8b
0x0041a8c3: add bh, bh
0x0041a8c5: db 0xff
0x0041a8c6: jmp far dword [rdi+0x2]
--
0x0041a8fd: add [rax], al
0x0041a8ff: add [rbx+0x0], bl
0x0041a902: add [rax], al
0x0041a904: db 0xfe
0x0041a905: db 0xff
0x0041a906: jmp far dword [rdi+0x2]

